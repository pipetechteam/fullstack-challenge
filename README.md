Pipetech
Full Stack Developer Code Challenge
===================

O objetivo do challenge é desenvolver um sistema de cadastro de alunos. 
Um (crud) de alunos. A Aplicação deve permitir listar, cadastrar, editar e excluir um aluno.


# Especificações Técnicas
- **Front End:** React.js
- **API:** Express e NodeJS
- **Banco de Dados:** PostgreSQL
- **Código e commits:** Inglês


## Campos obrigatórios:
- **Nome** (editável)
- **Email** (editável)
- **Matrícula** (não editável) (chave única)
- **CPF** (não editável)


# Critérios de avaliação
- Qualidade de escrita do código
- Organização do projeto
- Lógica da solução implementada
- Utilização do Git (quantidade e descrição dos commits, Git Flow, ...)


# Entrega 
Crie um repositório público no GitLab para realizar seu trabalho, quando finalizar só enviar o link do repositório para:
contato@pipetech.com.br
